CREATE TABLE activities (
	id INT NOT NULL,
	name VARCHAR (255),
	description VARCHAR(600),
	start DATE NOT NULL,
	finish DATE,
	status_id INT NOT NULL,
	situation INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (status_id) REFERENCES status(id)
);

CREATE TABLE status (
	id INT NOT NULL,
	name VARCHAR(24) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO status VALUES
	(1, 'Pendente'),
	(2, 'Em Desenvolvimento'),
	(3, 'Em Teste'),
	(4, 'Concluído');