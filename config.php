<?php 
define('BASEPATH', __DIR__);
define('BASEURL', 'http://localhost/project');

define('DBHOST', 'localhost');
define('DBNAME', 'projeto');
define('DBUSER', 'root');
define('DBPASS', '');

if(!function_exists('class_file_autoload')) {
	/**
	 * Classe para fazer a inclusão dos arquivos de objeto automaticamente.
	 * @param  string 	class_name 		Nome da classe que deseja importar no arquivo.
	 * @author Rodrigo Alves <rodrigo@alves.co.de>
	 */
	function class_file_autoload($class_name) {
		$class_name 	= strtolower($class_name);

		if(file_exists(BASEPATH . "/app/model/$class_name.class.php"))
			include_once BASEPATH . "/app/model/$class_name.class.php";
	}
}

spl_autoload_register('class_file_autoload');

if(!function_exists('sanitize')) {
	/**
	 * Função responsável por limpar a frase para evitar SQL injection.
	 * @param 	string 		sql 		Valor que deseja limpar.
	 * @return 	string 		Valor limpo
	 * @version 1.0
	 */
	function sanitize($sql) {
		$sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '', $sql);
		$sql = str_replace(array('<br>', '<BR>', '<br/>', '<BR/>'), "\n", $sql);
		$sql = trim($sql);
		$sql = strip_tags($sql);
		$sql = (get_magic_quotes_gpc()) ? $sql : addslashes($sql);

		return $sql;
	}
}
?>