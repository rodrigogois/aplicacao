<?php
if(!defined('BASEPATH'))
	include '../../config.php';

/**
 * Arquivo de rotinas da página home.
 * @param  	string 		action 		Parâmetro informado via GET. Ação que deseja efetuar.
 * @return 	string 		String no formato JSON, com resultado da ação.
 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
 * @version 1.0
 */

$action 	= isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	/**
	 * Rotina disparada quando parãmetro action é igual a 'load'. Responsável por construir a consulta e retornar json com lista de atividades.
	 * @param 	int 		activity[status]		Parâmetro submetido via POST. Status da atividade.
	 * @param 	int 		activity[situation]		Parâmetro submetido via POST. Situação da atividade.
	 * @return 	string 		String no formato JSON, com resultado da ação.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	case 'load':
		$where 		= array();
		$status		= array();
		
		$page 		= new Page;
		$statusdao	= new StatusDao;
		$_status	= $statusdao->get();

		foreach ($_status as $value) {
			$status[$value->get_id()] 	= $value; 
		}

		if(isset($_POST['activity']['status']) && is_numeric($_POST['activity']['status']) && $_POST['activity']['status']!='-1') {
			$where[] 	= array(
				'field' 	=> 'a.status_id',
				'operator' 	=> '=',
				'value' 	=> "'{$_POST['activity']['status']}'",
			);
		}

		if(isset($_POST['activity']['situation']) && is_numeric($_POST['activity']['situation']) && $_POST['activity']['situation']!='-1') {
			$where[] 	= array(
				'field' 	=> 'a.situation',
				'operator' 	=> '=',
				'value' 	=> "'{$_POST['activity']['situation']}'",
			);
		}

		if($where) {
			$where 	= array(
				'filters' 	=> $where
			);
		}else{
			$where 	= array();
		}

		$activitydao 	= new ActivityDao;
		$activities		= $activitydao->get($where);

		$return 		= array();

		foreach ($activities as $key => $activity) {
			$return[]  	= array(
				'id'			=> $activity->get_id(),
				'name'			=> $activity->get_name(),
				'description'	=> substr($activity->get_description(), 0, 20) . '...',
				'status_id'		=> $activity->get_status_id(),
				'status_name'	=> $status[$activity->get_status_id()]->get_name(),
				'situation'		=> $activity->get_situation(false),
				'start'			=> $activity->get_start('d/m/Y'),
				'finish'		=> $activity->get_finish('d/m/Y'),
				'url'			=> $page->url('index.php?route=activity/edit&id=' . $activity->get_id()),
			);
		}

		echo json_encode(
			array(
				'success'	=> true,
				'data'		=> array(
					'activities'	=> $return,
				)
			)
		);
		break;
	
	/**
	 * Rotina disparada na omissão do parãmetro action. Responsável por carregar as informações necessárias no carregamento da página.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	default:
		$activitydao 	= new ActivityDao;
		$activities		= $activitydao->get();

		$statusdao		= new StatusDao;
		$_status		= $statusdao->get();
		$status			= array();

		foreach ($_status as $value) {
			$status[$value->get_id()] 	= $value; 
		}

		ksort($status);

		break;
}
?>