<?php
if(!defined('BASEPATH'))
	include '../../config.php';

/**
 * Arquivo de rotinas da página activity/new e activity/edit.
 * @param  	string 		action 		Parâmetro informado via GET. Ação que deseja efetuar.
 * @return 	string 		String no formato JSON, com resultado da ação.
 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
 * @version 1.0
 */

$action 	= isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	/**
	 * Rotina disparada quando parãmetro action é igual a 'set'. Responsável por salvar a atividade submetida.
	 * @return 	string 		String no formato JSON, com resultado da ação.
	 * @param 	int 		activity[id]			Parâmetro submetido por POST. Id da atividade.
	 * @param 	int 		activity[name]			Parâmetro submetido por POST. Nome da atividade.
	 * @param 	int 		activity[status]		Parâmetro submetido por POST. Status da atividade.
	 * @param 	int 		activity[situation]		Parâmetro submetido por POST. Situação da atividade.
	 * @param 	int 		activity[start]			Parâmetro submetido por POST. Início da atividade.
	 * @param 	int 		activity[finish]		Parâmetro submetido por POST. Fim da atividade.
	 * @param 	int 		activity[description]	Parâmetro submetido por POST. descrição da atividade.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	case 'set':
		$page 		= new Page;
		$dao 		= new activityDao;
		$activity 	= new Activity;

		if(isset($_POST['activity']['id']) && !empty($_POST['activity']['id']))
			$activity->set_id($_POST['activity']['id']);

		$activity->set_name(isset($_POST['activity']['name']) ? $_POST['activity']['name'] : false);
		$activity->set_status_id(isset($_POST['activity']['status']) ? $_POST['activity']['status'] : false);
		$activity->set_situation(isset($_POST['activity']['situation']) ? $_POST['activity']['situation'] : false);
		$activity->set_start(isset($_POST['activity']['start']) ? $_POST['activity']['start'] : false);
		$activity->set_finish(isset($_POST['activity']['finish']) ? $_POST['activity']['finish'] : false);
		$activity->set_description(isset($_POST['activity']['description']) ? $_POST['activity']['description'] : false);

		if($activity->get_status_id()==4 && !$activity->get_finish()) {
			echo json_encode(
				array(
					'success' 	=> false,
					'message'	=> 'As atividade com o status igual a "Concluído", devem ter a data de fim informada.',
					'url' 		=> false,
				)
			);

			return false;
		}

		try {
			$result 	= $dao->set($activity);	
		} catch (Exception $e) {
			echo json_encode(
				array(
					'success' 	=> false,
					'message'	=> "Erro ao salvar atividade. ({$e->getMessage()})",
					'url' 		=> false,
				)
			);

			return false;
		}
		
		echo json_encode(
			array(
				'success' 	=> true,
				'message'	=> 'Atividade salva com sucesso.',
				'url' 		=> $page->url('index.php?route=home&success=1'),
			)
		);
		break;
	/**
	 * Rotina disparada na omissão do parãmetro action. Responsável por carregar as informações necessárias no carregamento da página.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	default:
		$activity 	= new Activity;

		if(isset($_GET['id']) && is_numeric($_GET['id'])) {
			$dao 		= new activityDao;
			$result 	= $dao->get(
				array(
					'filters' 	=> array(
						array(
							'field' 	=> 'a.id',
							'operator' 	=> '=',
							'value' 	=> "'$_GET[id]'",
						)
					),
				)
			);

			if($result)
				$activity 	= $result[0];
		}

		$statusdao		= new StatusDao;
		$_status		= $statusdao->get();
		$status			= array();

		foreach ($_status as $value) {
			$status[$value->get_id()] 	= $value; 
		}

		ksort($status);

		break;
}
?>