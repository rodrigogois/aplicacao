<?php 
/**
 * Arquivo com páginas disponíveis no sistema.
 * @author  Rodrigo Alves <rodrigo@alves.co.de>
 * @version 1.0
 */

$css_default 		= array(
	BASEURL . '/assets/css/bootstrap.min.css',
);

$js_default 		= array(
	'<script type="text/javascript">var url = "' . BASEURL . '";</script>',
	BASEURL . '/assets/js/jquery.min.js',
	BASEURL . '/assets/js/bootstrap.min.js',
	BASEURL . '/assets/js/common.js',
);

$pages['home'] 		= array(
	'title'		=> 'Início',
	'location'	=> 'home',
	'type'		=> 'html5',
	'css'		=> array_merge(
		$css_default,
		array()
	),
	'js'		=> array_merge(
		$js_default,
		array(
			BASEURL . '/assets/js/home.js',
		)
	),
);

$pages['activity/new'] 		= array(
	'title'		=> 'Nova Atividade',
	'location'	=> 'activity/new',
	'type'		=> 'html5',
	'css'		=> array_merge(
		$css_default,
		array(
			BASEURL . '/assets/css/jquery.datetimepicker.css',
		)
	),
	'js'		=> array_merge(
		$js_default,
		array(
			BASEURL . '/assets/js/jquery.moment.js',
			BASEURL . '/assets/js/jquery.datetimepicker.js',
			BASEURL . '/assets/js/activity-new.js',
		)
	),
);

$pages['activity/edit'] 		= array(
	'title'		=> 'Editar Atividade',
	'location'	=> 'activity/edit',
	'type'		=> 'html5',
	'css'		=> array_merge(
		$css_default,
		array(
			BASEURL . '/assets/css/jquery.datetimepicker.css',
		)
	),
	'js'		=> array_merge(
		$js_default,
		array(
			BASEURL . '/assets/js/jquery.moment.js',
			BASEURL . '/assets/js/jquery.datetimepicker.js',
			BASEURL . '/assets/js/activity-new.js',
		)
	),
);

$pages['error/404'] 		= array(
	'title'		=> 'Página não Encontrada',
	'location'	=> 'error/404',
	'type'		=> 'html5',
	'css'		=> array_merge(
		$css_default,
		array()
	),
	'js'		=> array_merge(
		$js_default,
		array()
	),
);

$pages['error/no-database'] 		= array(
	'title'		=> 'Erro ao conectar o banco de dados',
	'location'	=> 'error/no-database',
	'type'		=> 'html5',
	'css'		=> array_merge(
		$css_default,
		array()
	),
	'js'		=> array_merge(
		$js_default,
		array()
	),
);
?>