<?php
if(!class_exists('QueryHandle')) {
	/**
	 * Objeto responsável por tratar os valores passados no métodos get dos objetos dao.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	class QueryHandle extends DB {
		private $where;
		private $limit;
		private $orderby;
		private $operator	= array('=', '!=', '<', '>', '<=', '>=', 'BETWEEN', 'IN', 'LIKE', 'NOT BETWEEN', 'NOT IN', 'NOT LIKE');
		private $comparator	= array('OR', 'AND', 'NOT');

		/**
		 * Método recebe todos os parâmetros passados pelo método get.
		 * @param 	array 		filters 		Array de filtros para a consulta
		 * @param 	array 		param 			Array de paramêtros das consultas (LIMIT, ORDER BY)
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		function __construct($filters = array(), $param = array()) {
			if(!is_array($filters))
				return false;

			if(!count($filters))
				return false;

			if(!isset($filters[0]))
				$filters 	= array($filters);

			$where 			= $this->get_filters($filters);
			$this->where	= !empty($where) ? 'WHERE ' . $where : '';
			$this->limit 	= '';

			if(isset($param['LIMIT']) && is_numeric($param['LIMIT']))
				$this->limit 	= "LIMIT $param[LIMIT]";

			$orderby 		= '';

			if(isset($param['ORDERBY'])) {
				$orderby 	= sanitize($param['ORDERBY']);

				if(!empty($orderby)) {
					$orderby 	= "ORDER BY $param[ORDERBY]";
				}
			}

			$this->orderby 	= $orderby;
		}
		
		/**
		 * Método recebe lista, chama o método get_fields para tratar lista de condições.
		 * @param 	array 		filters 				Lista com o operador e condições da consulta
		 * @param 	string 		filters[comparator] 	Operador que deve ser utilizado para os filtros (OR, AND)
		 * @param 	array 		filters[filters] 		Lista de condições para a consulta
		 * @return 	string 		Lista de condições concatenada com o operador da consulta
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0
		 */
		private function get_filters($filters = array()) {
			$where 		= array();

			foreach ($filters as $filter) {
				$comparator		= isset($filter['comparator']) ? $filter['comparator'] : 'AND';
				
				if(!in_array($comparator, $this->comparator))
					$comparator 	= 'AND';

				if(!isset($filter['filters']))
					continue;

				if(!isset($filter['filters'][0]))
					$filter['filters'] 		= array($filter['filters']);

				$where 			= array_merge($where, $this->get_fields($filter['filters']));
			}

			return $where ? '(' . implode(" $comparator ", $where) . ')' : '';
		}
		
		/**
		 * Método recebe a lista de filtros da consulta e retorna lista de condições tratatos e formatados ([campo] [operador logico] [valor])
		 * @param 	array 		fields[n]				Lista de campos para a consulta 
		 * @param 	string 		fields[n][field]		Campo buscado
		 * @param 	string 		fields[n][operator]		Operador lógico (=, !=, <, >, [...])
		 * @param 	string 		fields[n][value]		Valor
		 * @param 	array 		fields[n][filters]		Caso seja informado, indice será tratado pelo método get_filters.
		 * @return 	array 		Lista de valores tratatos e formatados ([campo] [operador logico] [valor])
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		private function get_fields($fields = array()) {
			$where 		= array();

			foreach ($fields as $key => $val) {
				if(isset($val['filters'])) {
					$where[] 	= $this->get_filters(array($val));
				}else{
					$operator 	= isset($val['operator']) && in_array($val['operator'], $this->operator) ? $val['operator'] : '=';

					if(strpos($val['field'], '+')!==false) {
						$fields 	= explode('+', $val['field']);

						foreach ($fields as $k => $j) {
							$_field 		= trim($fields[$k]);

							if(empty($_field))
								unset($fields[$k]);
						}

						$_field		= count($fields) ? implode(' + ', $fields) : 'ID';
					}else{
						$_field 	= trim($val['field']);
						$_field		= empty($_field) ? 'ID' : $_field;
					}

					switch ($operator) {
						case 'IN':
						case 'NOT IN':
							if(preg_match('/\((.+)\)/', $val['value'], $matchin)) {
								$_infilters	= explode(',', $matchin[1]);
								$infilters 	= array();

								foreach ($_infilters as $_infilter) {
								 	$_infilter 		= trim($_infilter);
								 	$_infilter 		= stripslashes(sanitize($_infilter));
								 	$infilters[]	= $_infilter;
								}

								$value 			= '(' . implode(', ', $infilters) . ')';
							}else{
								$value 			= '(' . stripslashes(sanitize($val['value'])) . ')';
							}
							break;

						case 'BETWEEN':
							if(preg_match('/(.+) AND (.+)/', $val['value'], $match)) {
								$match[1] 		= stripslashes(sanitize($match[1]));
								$match[2] 		= stripslashes(sanitize($match[2]));

								$value 			= "UPPER($match[1]) AND UPPER($match[2])";
							}else{
								$value 			= stripslashes(sanitize($val['value']));
							}
							break;
						
						default:
							$val['value']	= stripslashes(sanitize($val['value']));
							$value 			= "UPPER($val[value])";
							break;
					}

					$where[] 		= "UPPER($_field) $operator $value";
				}
			}

			return 	$where;
		}
		
		/**
		 * Método responsável por retornar o atributo where do objeto.
		 * @return 	string 		String formatada na sintaxe SQL, WHERE [condições]
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_where() {
			return $this->where;
		}
		
		/**
		 * Método responsável por retornar o atributo limit do objeto.
		 * @return 	string 		String formatada na sintaxe SQL, LIMIT [valor]
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */		
		public function get_limit() {
			return $this->limit;
		}
		
		/**
		 * Método responsável por retornar o atributo orderby do objeto.
		 * @return 	string 		String formatada na sintaxe SQL, ORDER BY [campos] ASC/DESC
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */		
		public function get_orderby() {
			return $this->orderby;
		}

	}
}
?>