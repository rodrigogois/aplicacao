<?php 
if(!class_exists('ObjectUtils')) {
	/**
	 * Objeto com métodos de validações especificas, útils para os objetos.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 4.6
	 */
	class ObjectUtils {
		/**
		 * Validação para campos com valores númericos inteiros.
		 * @param 	int|float|string 	value 		Valor que deseja limpar.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 	 * @version 4.6
		 */
		public function integer($value) {
			if(!is_numeric($value))
				$value 		= (int) preg_replace('/(?!(\d))(.)/', '', $value);

			return (int) number_format($value, 0, '', '');
		}

		/**
		 * Validação para campos do tipo float. Método utiliza mesmos paramêtros da função number_format do PHP.
		 * @param 	string 		value			Valor a ser validado
		 * @param 	string 		decimals		Quantidade de casas decimais
		 * @param 	string 		dec_point		Separador de casas decimais
		 * @param 	string 		thousands_sep	Separador de milhares
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 	 * @version 4.6
		 */
		public function float($value, $decimals = 2, $dec_point = '.', $thousands_sep = '') {
			if(!is_numeric($value)) {
				$value 		= str_replace(',', '.', $value);
				$value 		= preg_replace('/(?!(\d)|\.)(.)/', '', $value);
			}

			return (float) number_format($value, $decimals, $dec_point, $thousands_sep);
		}

		/**
		 * Validação para campos com valores do tipo data.
		 * @param 	int|float|string 	value 		Valor que deseja limpar.
		 * @param 	string 				format 		Formato que a data deve ser retornada.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 	 * @version 4.6
		 */
		public function date($value, $format = 'Ymd') {
			if(preg_match('/^([\d]{2})\/([\d]{2})\/([\d]{2}|[\d]{4})$/', $value, $match)) {
				if(strlen($match[3])==2)
					$match[3] 	= "20$match[3]";

				$value 		= $match[3] . $match[2] . $match[1];
			}elseif(preg_match('/^([\d]{4})\-([\d]{2})\-([\d]{2})$/', $value, $match)) {
				$value 		= $match[1] . $match[2] . $match[3];
			}else{
				$value 		= preg_replace('/(?!(\d))(.)/', '', $value);
			}

			if(empty($value))
				return '';
			
			return checkdate(substr($value, 4, 2), substr($value, 6, 2), substr($value, 0, 4)) ? date($format, strtotime($value)) : '';
		}

		/**
		 * Validação para campos com valores do tipo string.
		 * @param 	int|float|string 	value 		Valor que deseja limpar.
		 * @param 	string 				format 		Formato que a data deve ser retornada.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 	 * @version 4.6
		 */
		public function string($value, $length = 1, $sanitize = true) {
			return substr(addslashes(($sanitize ? sanitize($value) : $value)), 0, $length);
		}
	}
}
?>