<?php 
if(!class_exists('ActivityDao')) {
	/**
	 * Classe de acesso a informações do objeto Activity (atividade).
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	class ActivityDao extends DB {
		function __construct() {
			$qr 	= array(
				'type'	=> 'select',
				'query'	=>
				"SELECT count(*) AS quantity 
				FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_NAME = 'activities'"
			);

			$result	= $this->query($qr);

			if($result) {
				if(!$result[0]['quantity']) {
					$qr 	= array(
						'type'	=> 'CREATE',
						'query'	=>
						"CREATE TABLE activities (
							id INT NOT NULL,
							name VARCHAR (255),
							description VARCHAR(600),
							start DATE NOT NULL,
							finish DATE,
							status_id INT NOT NULL,
							situation INT NOT NULL,
							PRIMARY KEY (id),
							FOREIGN KEY (status_id) REFERENCES status(id)
						);"
					);

					$this->query($qr);
				}
			}
		}

		/**
		 * Método consulta os fornece no BD.
		 * @param 	array 		filters 		Filtros da consulta na base de dados. Consulte documentação do objeto QueryHandle para mais informações.
		 * @param 	array 		param 			Parametros da consulta na base de dados (LIMIT, ORDERBY).
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0
		 */
		public function get($filters = array(), $param = array()) {
			if(!is_array($filters))
				return false;

			$handle 	= new QueryHandle($filters, $param);
			$query 		= array(
				'type' 		=> 'select',
				'query' 	=> 
				"SELECT a.id, a.name, a.description, a.start
					, a.finish, a.status_id, a.situation
				FROM activities a
				{$handle->get_where()}
				{$handle->get_orderby()}
				{$handle->get_limit()}"
			);
			
			$registers 	= $handle->query($query);

			if(!$registers)
				return array();

			foreach ($registers as &$register) {
				$register	= new Activity($register);
			}

			return $registers;
		}

		/**
		 * Método responsável por inserir, ou atualizar, um registro na base da dados.
		 * @param 	array 		activities 		Array de objetos do tipo Activity.
		 * @return 	int|bool 	Caso seja inserido, ou atualizado, somente um registro a função retorna o ID em caso de sucesso, no caso de sucesso em mais de uma ação, é retornado true. Caso haja falha, é retornado false.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0
		 */
		public function set($activities = array()) {
			if(!is_array($activities))
				$activities 	= array($activities);

			$recno		= false;
			$updates	= array();
			$inserts 	= array();

			foreach ($activities as $key => $activity) {
				if(get_class($activity) != 'Activity')
					continue;

				$is_update 		= $activity->get_id() ? true : false;

				if($is_update) {
					$id 		= $activity->get_id();
					$fields 	= array();

					if($activity->get_name()!==false)
						$fields[] 	= "name = '{$activity->get_name()}'";

					if($activity->get_description()!==false)
						$fields[] 	= "description = '{$activity->get_description()}'";

					if($activity->get_start()!==false)
						$fields[] 	= "start = '{$activity->get_start()}'";

					if($activity->get_finish()!==false)
						$fields[] 	= 'finish = ' . ($activity->get_finish() ? "'{$activity->get_finish()}'" : 'NULL');

					if($activity->get_situation()!==false)
						$fields[] 	= "situation = '{$activity->get_situation()}'";

					if($activity->get_status_id()!==false)
						$fields[] 	= "status_id = '{$activity->get_status_id()}'";

					$fields 	= implode(', ', $fields);
					$updates[] 	= "UPDATE activities SET $fields WHERE id = '{$activity->get_id()}'";
				}else{
					$fields 	= array();
					$values 	= array();

					if($recno) {
						$activity->set_id(++$recno);
					}else{
						$register 	= $this->query(
							array(
								'type'		=> 'select',
								'query' 	=> "SELECT MAX(id) as id FROM activities"
							)
						);

						if(!$register)
							return false;

						$recno 		= ($register[0]['id'] ? $register[0]['id'] : 0) + 1;
						$activity->set_id($recno);
					}

					if($activity->get_id()!==false) {
						$fields[] 	= 'id';
						$values[] 	= "'{$activity->get_id()}'";
					}else{
						throw new Exception("Atributo 'id' ausente no índice $key.");
					}

					if($activity->get_name()!==false) {
						$fields[] 	= 'name';
						$values[] 	= "'{$activity->get_name()}'";
					}else{
						throw new Exception("Atributo 'name' ausente no índice $key.");
					}

					if($activity->get_description()!==false) {
						$fields[] 	= 'description';
						$values[] 	= "'{$activity->get_description()}'";
					}else{
						throw new Exception("Atributo 'description' ausente no índice $key.");
					}

					if($activity->get_start()!==false) {
						$fields[] 	= 'start';
						$values[] 	= "'{$activity->get_start()}'";
					}else{
						throw new Exception("Atributo 'start' ausente no índice $key.");
					}

					if($activity->get_finish()!==false) {
						$fields[] 	= 'finish';
						$values[] 	= $activity->get_finish() ? "'{$activity->get_finish()}'" : 'NULL';
					}

					if($activity->get_situation()!==false) {
						$fields[] 	= 'situation';
						$values[] 	= "'{$activity->get_situation()}'";
					}else{	
						throw new Exception("Atributo situation ausente no índice $key.");
					}

					if($activity->get_status_id()!==false) {
						$fields[] 	= 'status_id';
						$values[] 	= "'{$activity->get_status_id()}'";
					}else{
						throw new Exception("Atributo 'status_id' ausente no índice $key.");
					}

					$where 		= array();

					foreach ($fields as $key => $field)
						$where[$field] = "$field = {$values[$key]}";

					$fields 	= implode(', ', $fields);
					$values 	= implode(', ', $values);
					$inserts[] 	= "INSERT INTO activities ($fields) VALUES ($values)";
				}
			}

			$insert 	= true;
			$update 	= true;

			if($inserts) {
				$insert 	= $this->query(
					array(
						'type'		=> 'insert',
						'query'		=> $inserts
					)
				);
			}

			if($updates) {
				$update 	= $this->query(
					array(
						'type'		=> 'update',
						'query'		=> $updates
					)
				);
			}

			if(isset($where)) {	
				$where 			= implode(' AND ', $where);
				$result 		= $this->query(
					array(
						'type'	=> 'select',
						'query'	=> 
						"SELECT id 
						FROM activities 
						WHERE $where
						ORDER BY id DESC"
					)
				);

				$id 			= isset($result[0]['ID']) ? $result[0]['ID'] : false;
			}

			if($insert && $update) {
				if(count($inserts) + count($updates) == 1) {
					return $id;
				}else{
					return true;
				}
			}else{
				return false;
			}
		}
	}
}
?>