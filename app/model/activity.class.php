<?php 
if(!class_exists('Activity')) {
	/**
	 * Objeto atividade (Activity).
	 * @param 	int 		data[id]				ID da atividade
	 * @param 	string 		data[name]				Nome da atividade
	 * @param 	string 		data[description]		Descrição da atividade
	 * @param 	string 		data[start]				Início da atividade
	 * @param 	string 		data[finish]			Final da atividade
	 * @param 	int 		data[status_id]			ID do status da atividade
	 * @param 	int 		data[situation]			Situação da atividade
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version	1.0
	 */
	class Activity extends ObjectUtils {
		private $id;
		private $name;
		private $description;
		private $start;
		private $finish;
		private $status_id;
		private $situation;

		function __construct($data = array()) {
			$this->set_id(isset($data['id']) ? $data['id'] : false);
			$this->set_name(isset($data['name']) ? $data['name'] : false);
			$this->set_description(isset($data['description']) ? $data['description'] : false);
			$this->set_start(isset($data['start']) ? $data['start'] : false);
			$this->set_finish(isset($data['finish']) ? $data['finish'] : false);
			$this->set_status_id(isset($data['status_id']) ? $data['status_id'] : false);
			$this->set_situation(isset($data['situation']) ? $data['situation'] : false);
		}

		/**
		 * Método responsável por atribuir valor para atributo id do objeto.
		 * @param 	int 		value 		Valor que deseja atribuir no atributo id
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_id($value = false) {
			$this->id			= $value!==false ? $this->integer($value) : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo name do objeto.
		 * @param 	string 		value 		Valor que deseja atribuir no atributo name
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_name($value = false) {
			$this->name			= $value!==false ? $this->string($value, 255) : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo description do objeto.
		 * @param 	string 		value 		Valor que deseja atribuir no atributo description
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_description($value = false) {
			$this->description	= $value!==false ? $this->string($value, 600) : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo start do objeto.
		 * @param 	string 		value 		Valor que deseja atribuir no atributo start
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_start($value = false) {
			$this->start		= $value!==false ? $this->date($value, 'Y-m-d') : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo finish do objeto.
		 * @param 	string 		value 		Valor que deseja atribuir no atributo finish
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_finish($value = false) {
			$this->finish		= $value!==false ? $this->date($value, 'Y-m-d') : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo status_id do objeto.
		 * @param 	int 		value 		Valor que deseja atribuir no atributo status_id
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_status_id($value = false) {
			$this->status_id	= $value!==false ? $this->integer($value) : false; 
		}

		/**
		 * Método responsável por atribuir valor para atributo situation do objeto.
		 * @param 	int 		value 		Valor que deseja atribuir no atributo situation
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_situation($value = false) {
			$this->situation	= $value!==false ? $this->integer($value) : false; 
		}

		/**
		 * Método responsável por retornar o valor do atributo id.
		 * @return 	int 		Valor do atributo id.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_id() {
			return $this->id;
		}

		/**
		 * Método responsável por retornar o valor do atributo name.
		 * @return 	string 		Valor do atributo name.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_name() {
			return $this->name;
		}

		/**
		 * Método responsável por retornar o valor do atributo description.
		 * @return 	string 		Valor do atributo description.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_description() {
			return $this->description;
		}

		/**
		 * Método responsável por retornar o valor do atributo start.
		 * @param 	string 		format 		Formato que a data deve ser retornada. Se o atributo estiver vazio, não é tratado.
		 * @return 	string 		Valor do atributo start.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_start($format = false) {
			return $format ? (empty($this->start) ? '' : date($format, strtotime($this->start))) : $this->start;
		}

		/**
		 * Método responsável por retornar o valor do atributo finish.
		 * @param 	string 		format 		Formato que a data deve ser retornada. Se o atributo estiver vazio, não é tratado.
		 * @return 	string 		Valor do atributo finish.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_finish($format = false) {
			return $format ? (empty($this->finish) ? '' : date($format, strtotime($this->finish))) : $this->finish;
		}

		/**
		 * Método responsável por retornar o valor do atributo status_id.
		 * @return 	int 		Valor do atributo status_id.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_status_id() {
			return $this->status_id;
		}

		/**
		 * Método responsável por retornar o valor do atributo situation.
		 * @return 	int 		Valor do atributo situation.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		public function get_situation($raw = true) {
			if($raw) {
				return $this->situation;
			}else{
				switch ($this->situation) {
					case '0':
						return 'Inativo';
						break;
					
					case '1':
						return 'Ativo';
						break;
					
					default:
						return $this->situation;
						break;
				}
			}
		}
	}
}
?>