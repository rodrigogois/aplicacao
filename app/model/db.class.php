<?php
if(!class_exists('DB')) {
	/**
	 * Classe responsável por fazer toda a interação com o banco de dados.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0.0
	 */
	class DB {
		private $link;

		/**
		 * Método responsável por envocar o metodo de conexão com o banco de dados.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		function __construct(){
			$this->link 		= $this->connect();
		}

		/**
		 * Método responsável por desfazer a conexão com o banco de dados.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		function __destruct(){
			$this->link = null;
		}

		/**
		 * Método responsável por fazer a conexão com o banco de dados.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		private function connect() {
			$pdo 	= null;
			$pdo 	= new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME . ';charset=UTF8;', DBUSER, DBPASS);
			
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
		}

		/**
		 * Método responsável por executar uma query no banco de dados.
		 * @param 	string 			qr[type] 		Tipo da consulta (SELECT, UPDATE, DELETE)
		 * @param 	string|array 	qr[query] 		Array de strings ou string, com a query que deseja executar
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @return 	bool|array 
		 * @version 1.0.0
		 */
		public function query(array $qr){
			if(!$this->link)
				$this->link 	= $this->connect();

			if(!isset($qr['query']) || empty($qr['query']))
				return false;

			if(!isset($qr['type']) || empty($qr['type']))
				return false;

			switch ($qr['type']) {
				case 'select':
					try{
						$st 	= $this->link->prepare($qr['query']);

						if($st->execute()) {
							$result 	= $this->onlyAssoc($st->fetchAll());
							$result 	= $this->trimArray($result);

							if(empty($result)) {
								return false;
							}else{
								return $result;
							}
						}else{
							return false;
						}
					}catch(Exception $e) {
						$log 	= fopen(BASEPATH . '/serverlogs/db_errors.log', 'a');

						if(!$log)
							return false;

						fwrite($log, '(' . date('Y-m-d H:i:s') . ')' . $e . PHP_EOL . PHP_EOL);
						fwrite($log, $qr['query']);
						fwrite($log, PHP_EOL . PHP_EOL);
						fclose($log);

						return false;
					}
					break;
				
				default:
					try{
						$check 			= 0;
						
						$this->link->beginTransaction();

						if(!is_array($qr['query'])) {
							$qr['query'] 	= array($qr['query']);
						}

						foreach($qr['query'] as $query) {
							$check 			= $this->link->exec($query);
						}
						
						$this->link->commit();

						return $check;				  
					}catch(Exception $e) {
						$this->link->rollBack();
						echo 'Erro: ' . $e->getMessage();
					}
					break;
			}
		}
		
		

		private function onlyAssoc(array $assoc) {
			$count = 0;

			foreach($assoc as $key){
				for($i=0;$i<=((count($key))/2);$i++){
					unset($assoc[$count][$i]);
				}
				$count++;
			}

			return $assoc;
		}

		private function trimArray($input) {
		    if(!is_array($input)){
		    	if(is_string($input)){
		        	return trim($input);
		        }
			}else{
		    	return array_map(array($this, 'trimArray'), $input);
		    }
		}
	}
}
?>