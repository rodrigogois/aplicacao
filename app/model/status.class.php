<?php 
if(!class_exists('status')) {
	/**
	 * Objeto Status.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version	1.0
	 */
	class Status extends ObjectUtils {
		private $id;
		private $name;

		function __construct($data = array()) {
			$this->set_id(isset($data['id']) ? $data['id'] : false);
			$this->set_name(isset($data['name']) ? $data['name'] : false);
		}
		
		/**
		 * Método responsável por atribuir valor ao atributo id.
		 * @param 	int 		id 		Valor que deseja atribuir.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_id($value = false) {
			$this->id		= $value!==false ? $value : false;
		}
		
		/**
		 * Método responsável por atribuir valor ao atributo name.
		 * @param 	string 		name 	Valor que deseja atribuir.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function set_name($value = false) {
			$this->name		= $value!==false ? $value : false;
		}
		
		/**
		 * Método responsável por retornar valor do atributo id.
		 * @return 	int 		Valor do atributo id.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function get_id() {
			return $this->id;
		}
		
		/**
		 * Método responsável por retornar valor do atributo name.
		 * @return 	string 		Valor do atributo name.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version	1.0
		 */
		public function get_name() {
			return $this->name;
		}
	}
}
?>