<?php
if(!class_exists('Page')) {
	/**
	 * Classe responsável por construir as página, enfileirar arquivos de JS e CSS, definir se o sistema deve utilizar Header e Footer, e etc.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0.0
	 */
	class Page {
		private $pages;
		private $page;

		/**
		 * Método responsável por instanciar arquivo contendo todas as páginas do site.
		 * @param 	string 		route 		Página que o usuário esta acessando. Deve ser preenchido com valor do parametro $_GET['route']
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		function __construct($route = false) {
			if(!$route)
				$route 		= 'home';

			include BASEPATH . '/app/pages.php';

			if(!isset($pages))
				$pages 		= array();

			$this->pages 	= $pages;
			$this->page 	= isset($pages[$route]) ? $pages[$route] : $pages['error/404'];
		}

		/**
		 * Método responsável por pegar o conteúdo do arquivo que deve ser incluido.
		 * @param 	string 	archive		Nome do arquivo que deseja incluir.
		 * @return 	string 	Resultado do arquivo incluído.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0.0
		 */
		private function get_template($archive = false) {
			if(!$archive)
				return '';

			if(!is_file(BASEPATH . "/app/view/$archive.php"))
				return '';

			ob_start();

			if(is_file(BASEPATH . "/app/controller/$archive.php"))
				include BASEPATH . "/app/controller/$archive.php";

			include BASEPATH . "/app/view/$archive.php";
			$output 	= ob_get_contents();

			ob_end_clean();

			return $output;
		}

		/**
		 * Método responsável por retornar a url absoluta do location informado.
		 * @param 	string 		location 		Endereço para o qual deseja criar uma url absoluta.
		 * @return 	string 		Url absoluta
		 * @author 	Rodigo Alves <rodrigo@co.de>
		 * @version 1.0.0 
		 */
		public function url(string $location) {
			$location 	= substr($location, 0, 1)=='/' ? $location : "/$location";
			return BASEURL . $location;
		}

		/**
		 * Método toString da classe.
		 * @return 	string 	HTML da página desejada.
		 * @author 	Rodigo Alves <rodrigo@co.de>
		 * @version 1.0.0 
		 */
		function __toString() {
			$content 	= array();
			$class 		= array(); 	
 			$page 		= $this->page;
			$html 		= new Html($page['title']);
			$is_html 	= $page['type']=='html' || $page['type']=='html5';

			$html->doctype($page['type']);

			$content[] 	= $this->get_template('template-header');

			if($is_html) {
				$html->link($page['css']);
				$html->link($page['js']);
			}

			$content[] 	= $this->get_template(str_replace('/', '-', $page['location']));
			$content[] 	= $this->get_template('template-footer');

			$html->body('class="' . implode(' ', $class) . '"');
			return $html->display(implode('', $content));
		}
	}
}
?>