<?php 
if(!class_exists('StatusDao')) {
	/**
	 * Classe de acesso a informações do objeto Status.
	 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
	 * @version 1.0
	 */
	class StatusDao extends DB {
		function __construct() {
			$qr 	= array(
				'type'	=> 'select',
				'query'	=>
				"SELECT count(*) AS quantity 
				FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_NAME = 'status'"
			);

			$result	= $this->query($qr);

			if($result) {
				if(!$result[0]['quantity']) {
					$qr 		= array(
						'type'	=> 'CREATE',
						'query'	=>
						"CREATE TABLE status (
							id INT NOT NULL,
							name VARCHAR(24) NOT NULL,
							PRIMARY KEY (id)
						);"
					);

					$this->query($qr);
					
					$status_list 	= array();

					$status 		= new Status;
					$status->set_name('Pendente');
					$status_list[] 	= $status;

					$status 		= new Status;
					$status->set_name('Em Desenvolvimento');
					$status_list[] 	= $status;

					$status 		= new Status;
					$status->set_name('Em Teste');
					$status_list[] 	= $status;

					$status 		= new Status;
					$status->set_name('Concluído');
					$status_list[] 	= $status;

					$this->set($status_list);
				}
			}
		}

		/**
		 * Método consulta os fornece no BD.
		 * @param 	array 		filters 		Filtros da consulta na base de dados. Consulte documentação do objeto QueryHandle para mais informações.
		 * @param 	array 		param 			Parametros da consulta na base de dados (LIMIT, ORDERBY).
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0
		 */
		public function get($filters = array(), $param = array()) {
			if(!is_array($filters))
				return false;

			$handle 	= new QueryHandle($filters, $param);
			$query 		= array(
				'type' 		=> 'select',
				'query' 	=> 
				"SELECT s.id, s.name
				FROM status s
				{$handle->get_where()}
				{$handle->get_orderby()}
				{$handle->get_limit()}"
			);
			
			$registers 	= $handle->query($query);

			if(!$registers)
				return array();

			foreach ($registers as &$register) {
				$register	= new Status($register);
			}

			return $registers;
		}

		/**
		 * Método responsável por inserir, ou atualizar, um registro na base da dados.
		 * @param 	array 		activities 		Array de objetos do tipo Status.
		 * @return 	int|bool 	Caso seja inserido, ou atualizado, somente um registro a função retorna o ID em caso de sucesso, no caso de sucesso em mais de uma ação, é retornado true. Caso haja falha, é retornado false.
		 * @author 	Rodrigo Alves <rodrigo@alves.co.de>
		 * @version 1.0
		 */
		public function set($records = array()) {
			if(!is_array($records))
				$records 	= array($records);

			$recno		= false;
			$updates	= array();
			$inserts 	= array();

			foreach ($records as $key => $record) {
				if(get_class($record) != 'Status')
					continue;

				$is_update 		= $record->get_id();

				if($is_update) {
					$fields 	= array();

					if($record->get_name()!==false)
						$fields[] 	= "name = '{$record->get_name()}'";

					$fields 	= implode(', ', $fields);
					$updates[] 	= "UPDATE status SET $fields WHERE id = '{$record->get_id()}'";
				}else{
					$fields 	= array();
					$values 	= array();

					if($recno) {
						$record->set_id(++$recno);
					}else{
						$register 	= $this->query(
							array(
								'type'		=> 'select',
								'query' 	=> "SELECT MAX(id) as id FROM status"
							)
						);

						if(!$register)
							return false;

						$recno 		= ($register[0]['id'] ? $register[0]['id'] : 0) + 1;
						$record->set_id($recno);
					}

					if($record->get_id()!==false) {
						$fields[] 	= 'id';
						$values[] 	= "'{$record->get_id()}'";
					}else{
						throw new Exception("Atributo 'id' ausente no índice $key.");
					}

					if($record->get_name()!==false) {
						$fields[] 	= 'name';
						$values[] 	= "'{$record->get_name()}'";
					}else{
						throw new Exception("Atributo 'name' ausente no índice $key.");
					}

					$where 		= array();

					foreach ($fields as $key => $field)
						$where[$field] = "$field = {$values[$key]}";

					$fields 	= implode(', ', $fields);
					$values 	= implode(', ', $values);
					$inserts[] 	= "INSERT INTO status ($fields) VALUES ($values)";
				}
			}

			$insert 	= true;
			$update 	= true;

			if($inserts) {
				$insert 	= $this->query(
					array(
						'type'		=> 'insert',
						'query'		=> $inserts
					)
				);
			}

			if($updates) {
				$update 	= $this->query(
					array(
						'type'		=> 'update',
						'query'		=> $updates
					)
				);
			}

			if(isset($where)) {	
				$where 			= implode(' AND ', $where);
				$result 		= $this->query(
					array(
						'type'	=> 'select',
						'query'	=> 
						"SELECT id 
						FROM status 
						WHERE $where
						ORDER BY id DESC"
					)
				);

				$id 			= isset($result[0]['ID']) ? $result[0]['ID'] : false;
			}

			if($insert && $update) {
				if(count($inserts) + count($updates) == 1) {
					return $id;
				}else{
					return true;
				}
			}else{
				return false;
			}
		}
	}
}
?>