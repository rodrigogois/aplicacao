<div class="jumbotron">
	<div class="container">
		<h1><?php echo $this->page['title'] ?></h1>
		<p>Este é um projeto simples de cadastro de atividades.</p>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Atividades</h2>
			<form class="form-horizontal" id="home-search">
				<div class="row form-group">
					<div class="col-md-12" id="message-wrapper">
						<?php echo isset($_GET['success']) ? '<div class="alert alert-success">Atividade salva com sucesso.</div>' : '' ?>
					</div>
				</div>
				<div class="row form-group">
					<label for="activity-status" class="col-sm-1 control-label">Status</label>
					<div class="col-sm-3">
						<select name="activity[status]" class="activity-status form-control" id="activity-status">
							<option value="-1">Todos</option>
							<?php foreach ($status as $value) : ?>
							<option value="<?php echo $value->get_id() ?>"><?php echo $value->get_name() ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<label for="activity-situation" class="col-sm-1 control-label">Situação</label>
					<div class="col-sm-3">
						<select name="activity[situation]" class="activity-situation form-control" id="activity-situation">
							<option value="-1">Todos</option>
							<option value="0">Inativo</option>
							<option value="1">Ativo</option>
						</select>
					</div>
					<div class="col-sm-offset-2 col-sm-2">
						<a href="#" class="btn btn-block btn-primary btn-search">Buscar</a>
					</div>
				</div>
			</form>
			<hr>
			<div class="row form-group">
				<div class="col-md-12">
					<table class="table activity-list">
						<thead>
							<tr>
								<th class="text-center">ID</th>
								<th>Título</th>
								<th>Descrição</th>
								<th>Status</th>
								<th>Situação</th>
								<th>Início</th>
								<th>Fim</th>
								<th class="text-center">Editar</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($activities as $activity) : ?>
							<tr class="<?php echo $activity->get_status_id()==4 ? 'success' : '' ?>">
								<td class="text-center"><?php echo $activity->get_id() ?></td>
								<td><?php echo $activity->get_name() ?></td>
								<td><?php echo substr($activity->get_description(), 0, 20) ?>...</td>
								<td><?php echo $status[$activity->get_status_id()]->get_name() ?></td>
								<td><?php echo $activity->get_situation(false) ?></td>
								<td><?php echo $activity->get_start('d/m/Y') ?></td>
								<td><?php echo $activity->get_finish('d/m/Y') ?></td>
								<td>
									<a href="<?php echo $this->url('index.php?route=activity/edit&id=' . $activity->get_id()) ?>" class="btn btn-edit btn-xs btn-warning btn-block">Editar Atividade</a>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-10">
				<a class="btn btn-primary btn-block" href="<?php echo $this->url('index.php?route=activity/new') ?>" role="button">Nova Atividade</a>
			</div>
		</div>
	</div>
</div>