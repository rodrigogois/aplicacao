<div class="jumbotron">
	<div class="container">
		<h1><?php echo $this->page['title'] ?></h1>
		<p>Parece que houve um erro na conexão na base de dados. <br/>Por favor, certifique-se que as informações do arquivo <strong>config.php</strong> estão corretas.</p>
	</div>
</div>