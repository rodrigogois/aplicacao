<div class="jumbotron">
	<div class="container">
		<h1><?php echo $this->page['title'] ?></h1>
	</div>
</div>
<div class="container">
	<div class="row form-group">
		<div class="col-md-12" id="message-wrapper"></div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal" id="activity-form">
				<input type="hidden" name="activity[id]" class="activity-id" id="activity-id" value="<?php echo $activity->get_id() ?>">
				<div class="row form-group">
					<div class="field">
						<label for="activity-name" class="col-sm-1 control-label">Nome&nbsp;<span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<input type="text" name="activity[name]" class="activity-name form-control required" id="activity-name" maxlength="255" value="<?php echo $activity->get_name() ?>">
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="field">
						<label for="activity-status" class="col-sm-1 control-label">Status&nbsp;<span class="text-danger">*</span></label>
						<div class="col-sm-3">
							<select name="activity[status]" class="activity-status form-control required" id="activity-status">
								<?php foreach ($status as $value) : ?>
								<option value="<?php echo $value->get_id() ?>" <?php echo $activity->get_status_id()==$value->get_id() ? 'selected="selected"' : '' ?>><?php echo $value->get_name() ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="field">
						<label for="activity-situation" class="col-sm-1 control-label">Situação&nbsp;<span class="text-danger">*</span></label>
						<div class="col-sm-3">
							<select name="activity[situation]" class="activity-situation form-control required" id="activity-situation">
								<option value="1"<?php echo $activity->get_situation()=='1' ? ' selected="selected"' : '' ?>>Ativo</option>
								<option value="0"<?php echo $activity->get_situation()=='0' ? ' selected="selected"' : '' ?>>Inativo</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="field">
						<label for="activity-start" class="col-sm-1 control-label">Início&nbsp;<span class="text-danger">*</span></label>
						<div class="col-sm-3">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="glyphicon glyphicon-calendar"></i>
								</div>
								<input type="text" class="form-control activity-start required" id="activity-start" name="activity[start]" value="<?php echo $activity->get_start('d/m/Y') ?>">
							</div>
						</div>
					</div>
					<div class="field">
						<label for="activity-finish" class="col-sm-1 control-label">Fim&nbsp;<span class="text-danger hidden">*</span></label>
						<div class="col-sm-3">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="glyphicon glyphicon-calendar"></i>
								</div>
								<input type="text" class="form-control activity-finish" id="activity-finish" name="activity[finish]" value="<?php echo $activity->get_finish('d/m/Y') ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="field">
						<label for="activity-description" class="col-sm-1 control-label">Descrição&nbsp;<span class="text-danger">*</span></label>
						<div class="col-sm-7">
							<textarea rows="5" type="text" class="form-control activity-description required" id="activity-description" name="activity[description]" maxlength="600"><?php echo $activity->get_description() ?></textarea>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-12">
						<small>
							<span class="text-danger">*</span> Campos Obrigatórios
						</small>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-2">
						<a href="#" class="btn btn-block btn-success btn-save">salvar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>