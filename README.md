# CADASTRO DE ATIVIDADES #

Aplicação com cadastro de atividades.


### CONFIGURAÇÃO ###

Para configurar a aplicação, altere o arquivo **config.php** nas seguintes linhas:

```
define('BASEURL', 'http://localhost/project');

define('DBHOST', 'localhost');
define('DBNAME', 'projeto');
define('DBUSER', 'root');
define('DBPASS', '');
```

**BASEURL** - Deve conter a URL do diretório onde a aplicação está instalada.

**DBHOST** - Endereço de host do seu banco de dados.

**DBNAME** - Nome do banco de dados.

**DBUSER** - Usuário do banco de dados.

**DBPASS** - Senha do usuário da base de dados