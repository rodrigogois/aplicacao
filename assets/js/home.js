(function($) {
	$(document).ready(function() {
		$('.btn-search').on('click', function () {
			var form 	= $('#home-search').serializeArray();

			$.ajax({
				url		: url + '/app/controller/home.php?action=load',
				data 	: form,
				type 	: 'POST',
				success	: function(json) {
					try{
					   var json 	= JSON.parse(json);
					}catch(e) {
						show_message('Requisição inválida. Verifique o formulário e/ou tente novamente mais tarde.', 'danger');
						return false;
					}

					if(!json.success) {
						show_message(json.message, 'danger');
						return false;
					}

					show_message(false);
					$('.activity-list tbody').html('');

					$.each(json.data.activities, function(key, activity) {
						var html 	= [];

						html.push('<tr class="' + (activity.status_id==4 ? 'success' : '') + '">')
						html.push('<td class="text-center">' + activity.id + '</td>');
						html.push('<td>' + activity.name + '</td>');
						html.push('<td>' + activity.description + '</td>');
						html.push('<td>' + activity.status_name + '</td>');
						html.push('<td>' + activity.situation + '</td>');
						html.push('<td>' + activity.start + '</td>');
						html.push('<td>' + activity.finish + '</td>');
						html.push('<td><a href="' + activity.url + '" class="btn btn-edit btn-xs btn-warning btn-block">Editar Atividade</a></td>');
						html.push('</tr>');

						$('.activity-list tbody').append(html.join(''));
					})
				}
			})


			return false;
		})
	})
})(jQuery)