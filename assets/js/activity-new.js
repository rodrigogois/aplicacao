(function($) {
	$(document).ready(function() {
		$('.activity-status').on('change', function() {
			var field = $('.activity-finish').closest('.field');

			if($(this).val()=='4') {
				field.find('.control-label .text-danger').removeClass('hidden');
				field.find('.form-control').addClass('required');
			}else{
				field.find('.control-label .text-danger').addClass('hidden');
				field.find('.form-control').removeClass('required');
			}

		}).trigger('change');

		$('#activity-start').datetimepicker({
			lang				: 'pt',
			timepicker			: false,
			format				: 'd/m/Y',
			onChangeDateTime	: function(dp,_start){
				var _end 	= $('#activity-finish');
				var start	= new moment(_start.val(), 'DD/MM/YYYY');
				var end 	= new moment($('#activity-finish').val(), 'DD/MM/YYYY');

				if($('#activity-finish').val()) {
					if(!start.isValid() || end.isBefore(start))
						_start.val('');

					if(end.isBefore(start)) {
						_start.setOptions({
							maxDate	: end.format('YYYY/MM/DD')
						})
					}
				}
			},
			onShow				: function(ct) {
				var maxDate 	= $('#activity-finish').val() ? $('#activity-finish').val() : false;

				if(maxDate) {
					maxDate 		= new moment(maxDate, 'DD/MM/YYYY');
					maxDate 		= maxDate.format('YYYY/MM/DD');
				}

				this.setOptions({
					maxDate	: maxDate
				})
			}
		});

		$('#activity-finish').datetimepicker({
			lang				: 'pt',
			timepicker			: false,
			format				: 'd/m/Y',
			onChangeDateTime	: function(dp,_end){
				var _start 	= $('#activity-start');
				var end 	= new moment(_end.val(), 'DD/MM/YYYY');
				var start 	= new moment(_start.val(), 'DD/MM/YYYY');

				if(!end.isValid() || end.isBefore(start))
					_end.val('');

				if(end.isBefore(start)) {
					_end.setOptions({
						minDate	: start.format('YYYY/MM/DD')
					})
				}
			},
			onShow				: function(ct) {
				var minDate 	= $('#activity-start').val() ? $('#activity-start').val() : false;

				if(minDate) {
					minDate 		= new moment(minDate, 'DD/MM/YYYY');
					minDate 		= minDate.format('YYYY/MM/DD');
				}

				this.setOptions({
					minDate	: minDate
				})
			}
		});

		$('.btn-save').on('click', function() {
			var error 	= 0;

			$('.field.has-error').removeClass('has-error');
			show_message(false);

			$('.form-control.required').each(function() {
				if($(this).val()=='') {
					$(this).closest('.field').addClass('has-error');
					error++;
				}
			})

			if(error) {
				show_message('Preencha os campos obrigatórios', 'danger');
				return false;
			}

			var form 	= $('#activity-form').serializeArray();

			$.ajax({
				url		: url + '/app/controller/activity-new.php?action=set',
				data 	: form,
				type 	: 'POST',
				success	: function(json) {
					try{
					   var json 	= JSON.parse(json);
					}catch(e) {
						show_message('Requisição inválida. Verifique o formulário e/ou tente novamente mais tarde.', 'danger');
						return false;
					}

					show_message(json.message, json.success ? 'success' : 'danger');

					if(json.url)
						location.href 	= json.url;
				}
			});
		})
	})
})(jQuery);