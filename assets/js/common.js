function show_message(text, type) {
	text 	= typeof text == 'undefined' ? false : text;
	type 	= typeof type == 'undefined' ? 'danger' : type;

	if(!text) {
		$('#message-wrapper').html('');
		return false;
	}

	$('#message-wrapper').html('<div class="alert alert-' + type + '">' + text + '</div>')
}